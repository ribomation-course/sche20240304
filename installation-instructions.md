# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## Operating System
If you are using Windows, you must have WSL version 2 installed,
plus Ubuntu Linux installed.
* https://learn.microsoft.com/en-us/windows/wsl/install
* https://www.windowscentral.com/how-install-wsl2-windows-10
* https://linuxsimply.com/linux-basics/os-installation/wsl/ubuntu-on-wsl2/

For Mac and *NIX it's much simpler, just proceed.

## Tools
* Docker Desktop & CLI
  * https://www.docker.com/products/docker-desktop/
  * https://docs.docker.com/get-docker/
* GIT Client
    - https://git-scm.com/downloads
* BASH terminal, such as the Ubuntu terminal.
* Decent IDE, such as
    * MicroSoft Visual Code
      - https://code.visualstudio.com/
    * JetBrains WebStorm / Intellij / ...
      - https://www.jetbrains.com/webstorm/download
* SQL client, such as 
  * JetBrains DataGrip or
    - https://www.jetbrains.com/datagrip/
  * HeidiSQL
    - https://www.heidisql.com/
* Modern browser, such as any of
    - [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
    - [Mozilla FireFox](https://www.mozilla.org/sv-SE/firefox/new/)
    - [Microsoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)
* Google Cloud CLI
  * https://cloud.google.com/sdk/docs/install#deb 

## Accounts
* Docker account
  * https://hub.docker.com/signup
* GCP account
  * https://console.cloud.google.com/freetrial

