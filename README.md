# Docker & Docker Compose, 2 days
### 2024 March

Welcome to this course. Here you will find
* Installation instructions
* Solutions to the programming exercises
* Sources to the demo programs

_N.B._ Solutions and Demos programs will be pushed during the course.


# Links
* [Installation instructions](./installation-instructions.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/dev-tools/docker/)



Course GIT Repo
====
It's recommended that you keep the git repo and your solutions separated.
Create a dedicated directory for this course and a subdirectory for
each chapter. Get the course repo initially by a `git clone` operation

![Git Clone](img/git-clone.png)

    mkdir -p ~/docker-course/my-solutions
    cd ~/docker-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo, and you can get these by
a `git pull` operation

    cd ~/nodejs-course/gitlab
    git pull

# .NET Links
* [Docker C# - Hello World](https://github.com/alexfricker/docker-cs-hello-world)
* [.NET Development With Docker & Rider](https://www.jetbrains.com/guide/dotnet/tutorials/docker-dotnet/local-dotnet-development-docker/)
* [Containerize a .NET app](https://learn.microsoft.com/en-us/dotnet/core/docker/build-container?tabs=windows&pivots=dotnet-8-0)
* [Official images for .NET and ASP.NET Core](https://hub.docker.com/_/microsoft-dotnet/)
* [How to compile and run a csharp file using dockerfile](https://stackoverflow.com/questions/70564222/how-to-compile-and-run-a-csharp-file-using-dockerfile)
* [Build Your "Hello World" Container Using C#](https://developers.redhat.com/articles/c-containers)
* [Containerize the .NET Core 7 Console Application using Docker](https://www.c-sharpcorner.com/article/containerize-the-net-core-7-console-application-using-docker/)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
