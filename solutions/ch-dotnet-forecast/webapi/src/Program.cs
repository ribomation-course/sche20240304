var builder = WebApplication.CreateBuilder(args);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();
app.UseSwagger();
app.UseSwaggerUI();

var baseUrl = "http://localhost:8080";

var summaries = new[] {
    "Freezing", "Bracing", "Chilly", "Cool", "Mild",
    "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

app.MapGet("/", () => {
    var info = new Info(
        "Silly Fake Weather Forecast Service, v0.42",
        DateTime.Now,
        new string[] {
            baseUrl,
            baseUrl + "/api/forecast",
            baseUrl +"/api/forecast/10",
            baseUrl+"/swagger/index.html",
        }
    );

    return info;
})
.WithName("Info")
.WithOpenApi();

app.MapGet("/api/forecast", () => {
    var forecast = Enumerable
        .Range(1, 3)
        .Select(day => new Forecast(
            DateOnly.FromDateTime(DateTime.Now.AddDays(day)),
            Random.Shared.Next(-30, 30),
            summaries[Random.Shared.Next(summaries.Length)]
        ))
        .ToArray();

    return forecast;
})
.WithName("GetForecast3")
.WithOpenApi();

app.MapGet("/api/forecast/{count}", (int count) => {
    var forecast = Enumerable
        .Range(1, count)
        .Select(day => new Forecast(
            DateOnly.FromDateTime(DateTime.Now.AddDays(day)),
            Random.Shared.Next(-35, 35),
            summaries[Random.Shared.Next(summaries.Length)]
        ))
        .ToArray();

    return forecast;
})
.WithName("GetForecastN")
.WithOpenApi();


app.Run(baseUrl);

record Forecast(DateOnly Date, int TemperatureC, string? Summary) {
    public int TemperatureF => 32 + (int)((9.0 / 5) * TemperatureC);
}

record Info(string server, DateTime date, string[] url) { }
