# Build

    docker build -t silly-server:3.0 .

# Run

    docker run --rm -d -p 8000:8000 --name silly-server silly-server:3.0
    http :8000
    http :8000/echo name=nisse age:=42 female:=false

# Clean-Up

    docker stop silly-server
    docker rmi silly-server:3.0

