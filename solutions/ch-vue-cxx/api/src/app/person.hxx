#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <nlohmann/json.hpp>

namespace model {
    using std::string;
    using std::vector;
    using json = nlohmann::json;

    //id;name;age;email;city;country
    //1;Kat Dy;49;kdy0@sina.com.cn;Oskarshamn;Sweden
    struct Person {
        unsigned id{};
        string name{};
        unsigned short age{};
        string email{};
        string city{};
        string country{};

    };

    inline auto operator<<(std::ostream& os, Person const& p) -> std::ostream& {
        os << "Person{";
        os << p.id;
        os << ", " << p.name;
        os << ", " << p.age;
        os << ", " << p.email;
        os << ", " << p.city;
        os << ", " << p.country;
        os << "}";
        return os;
    }

    auto fromCSV(string const& csv) -> Person;

    auto fromCSV(std::istream& in) -> std::vector<Person>;

    inline void to_json(json& j, Person const& p) {
        j = json{
                {"id",      p.id},
                {"name",    p.name},
                {"age",     p.age},
                {"email",   p.email},
                {"city",    p.city},
                {"country", p.country},
        };
    }

    inline void from_json(json const& j, Person& p) {
        j.at("id").get_to(p.id);
        j.at("name").get_to(p.name);
        j.at("age").get_to(p.age);
        j.at("email").get_to(p.email);
        j.at("city").get_to(p.city);
        j.at("country").get_to(p.country);
    }

    struct PersonRepo {
        vector<Person> repo{};

        explicit PersonRepo(string const& filename) {
            using namespace std::string_literals;

            auto file = std::ifstream{filename};
            if (!file) {
                throw std::invalid_argument{"cannot open "s + filename};
            }
            repo = fromCSV(file);
        }

        auto toJSON() -> json { return repo; }

        auto toJSON(unsigned id) -> json {
            namespace r = std::ranges;
            using namespace std::string_literals;

            if (auto p = r::find_if(repo, [id](auto p) { return p.id == id; }); p != repo.end()) {
                return *p;
            }
            return {};
        }
    };

}




