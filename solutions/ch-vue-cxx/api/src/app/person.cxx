#include <iostream>
#include <string>
#include <string_view>
#include <vector>
#include <ranges>
#include "person.hxx"


namespace model {
    using namespace std::literals;
    using std::string;
    using std::string_view;
    using std::stoi;

    //id;name;age;email;city;country
    //1;Kat Dy;49;kdy0@sina.com.cn;Oskarshamn;Sweden
    //2;Hugh Fallen;50;hfallen1@addthis.com;Linköping;Sweden
    auto fromCSV(string const& csv) -> Person {
        constexpr auto delim{";"sv};
        auto fields = std::vector<string>{};
        fields.reserve(6);
        for (auto field: std::views::split(csv, delim)) {
            auto f = string{string_view{field}};
            fields.push_back(f);
        }

        auto p = Person{};
        auto ix = 0U;
        p.id = stoi(fields[ix++]);
        p.name = fields[ix++];
        p.age = stoi(fields[ix++]);
        p.email = fields[ix++];
        p.city = fields[ix++];
        p.country = fields[ix++];

        return p;
    }

    auto fromCSV(std::istream& in) -> std::vector<Person> {
        auto persons = std::vector<Person>{};
        persons.reserve(1024);

        bool first_line = true;
        for (string csv; std::getline(in, csv);) {
            if (first_line) {
                first_line = false;
                continue;
            }
            persons.push_back(fromCSV(csv));
        }

        persons.shrink_to_fit();
        return persons;
    }

}


