import {readFileSync} from 'node:fs';
import express from 'express';
import cors from 'cors';
import {DateTime} from 'luxon';

const port = process.env.PORT || 8080;
const name = 'Silly Server';

const web = express();
web.use(cors());

web.use(express.json({
    limit: '10kb',
    strict: true,
}));

const server = web.listen(port, () => {
    console.log(`${name} listening on port %o`, server.address().port);
});

web.get('/', (req, res) => {
    const pkg = readFileSync('package.json', 'utf8').toString();
    const version = JSON.parse(pkg).version;
    const date = DateTime.now().setLocale('sv').toFormat('MMMM yyyy HH:mm');
    res.json({ name, version, date });
});

web.post('/echo', (req, res) => {
    const payload = req.body;
    const reply = Object.entries(payload)
                        .map(([key,val]) => {
                            if (typeof val === 'string') {val = val.toUpperCase()}
                            if (typeof val === 'number') {val *= 10}
                            if (typeof val === 'boolean') {val = !val}
                            return [key,val];
                        });
    res.json({reply: Object.fromEntries(reply)});
});

