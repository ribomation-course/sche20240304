import {readFile, stat} from 'node:fs/promises';
import {hostname} from 'node:os';
import {cwd} from 'node:process';
import express, {Express, Request, Response} from 'express';
import cors from 'cors';

interface Language {
    id: string;
    name: string;
    year: number;
    inventor: string;
    country: string;
}

const PORT: number = Number(process.env.PORT || 8000);
let db: Language[] = [];

const app: Express = express();
app.use(cors());

app.get("/", (_req: Request, res: Response) => {
    res.json({
        name: 'The Programming Languages Directory',
        version: '1.0',
        date: new Date()
    });
});

app.get("/lang", (_req: Request, res: Response) => {
    res.json(db);
});

app.get("/lang/:id", (req: Request, res: Response) => {
    const id = req.params.id as string;
    const lang = db.find(l => l.id === id);
    if (!lang) {
        res.sendStatus(404);
    } else {
        res.json(lang);
    }
});


app.listen(PORT, async () => {
    const load = async (file: string) => {
        try {
            await stat(file);
            return await readFile(file, 'utf8');
        } catch (_) {
            return null;
        }
    };

    const dbFile = 'lang.db.json';
    let data = await load(`${cwd()}/${dbFile}`);
    if (!data) {
        data = await load(`${cwd()}/src/${dbFile}`);
        if (!data) {
            throw new Error('cannot find ' + dbFile);
        }
    }

    db = JSON.parse(data).languages;

    console.log(`LangDB server started at http://${hostname()}:${PORT}`);
});



