import { defineConfig } from 'vite';
import { ViteMinifyPlugin } from 'vite-plugin-minify';

export default defineConfig({
    logLevel: 'info',
    
    server: {
        watch: {
            usePolling: true
        }
    },

    plugins: [
        // input https://www.npmjs.com/package/html-minifier-terser options
        ViteMinifyPlugin({}),
      ],
})

