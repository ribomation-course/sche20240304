import {sortBy} from 'lodash-es';
import './style.css';

const HOST = import.meta.env.PROD ? 'lang_srv' : 'localhost';
const PORT = import.meta.env.PORT || '8000';
const baseUrl = `http://${HOST}:${PORT}/lang`;

interface Language {
    id: string;
    name: string;
    year: number;
    inventor: string;
    country: string;
}

async function findAll(): Promise<Language[]> {
    try {
        const res = await fetch(baseUrl);
        if (res.ok) {
            const list = await res.json();
            return sortBy(list, 'name');
        }
        console.error('not found');
        return [];
    } catch (err) {
        console.error('failed: ' + err);
        return [];
    }
}

function mkTR(nr:number, lang:Language) {
    const tr = document.createElement('tr');
    tr.innerHTML = `
        <td>${nr}</td>
        <td>${lang.name}</td>
        <td>${lang.inventor}</td>
        <td>${lang.year}</td>
        <td>${lang.country}</td>
    `;
    return tr;
}

async function main() {
    console.log('Base URL: %o', baseUrl);

    const langLst = await findAll();
    let nr = 1;

    const tbody = document.querySelector('#list');
    if (!!tbody) {
        tbody.innerHTML='';
        langLst.forEach(lang => {
            const tr = mkTR(nr++, lang);
            tbody.appendChild(tr);
        });
    }
}

try {
    (async () => { await main(); })();
} catch (err) { console.error(err); }



