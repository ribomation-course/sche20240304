#include <catch2/catch_test_macros.hpp>
#include <sstream>
#include <vector>
#include <ranges>
#include "person.hxx"

using namespace std::literals;

TEST_CASE("parse single CSV line") {
    auto const csv = "2;Hugh Fallen;50;hfallen1@addthis.com;Linköping;Sweden"s;
    auto p = model::fromCSV(csv);
    REQUIRE(p.id == 2);
    REQUIRE(p.age == 50);
    REQUIRE(p.name == "Hugh Fallen"s);
    REQUIRE(p.email == "hfallen1@addthis.com"s);
    REQUIRE(p.city == "Linköping"s);
    REQUIRE(p.country == "Sweden"s);
}

TEST_CASE("parse several CSV lines") {
    auto payload = R"(id;name;age;email;city;country
1;Kat Dy;49;kdy0@sina.com.cn;Oskarshamn;Sweden
2;Hugh Fallen;50;hfallen1@addthis.com;Linköping;Sweden
3;Rhea Kabsch;30;rkabsch2@redcross.org;Karlskrona;Sweden
4;Rafaelita Peaker;27;rpeaker3@sun.com;Stockholm;Sweden
5;Lacy MacKessock;49;lmackessock4@facebook.com;Göteborg;Sweden
)";

    auto buf = std::istringstream{payload};
    auto lst = model::fromCSV(buf);
    REQUIRE(lst.size() == 5);

    namespace r = std::ranges;
    namespace v = std::ranges::views;
    using std::vector;

    auto expected_ages = std::vector<short>{49, 50, 30, 27, 49};
    auto actual_ages = std::vector<short>{};
    for (auto age: lst | v::transform([](auto&& p) { return p.age; })) actual_ages.push_back(age);
    REQUIRE(actual_ages == expected_ages);
}

TEST_CASE("convert a person object to json") {
    using json = nlohmann::json;
    auto p = model::Person{17, "Anna Conda"s, 42, "anna@gmail.com"s, "Uppsala"s, "Sweden"s};
    json j = p;
    //std::cout << "JSON: " << j << "\n";
    //{"age":42,"city":"Uppsala","country":"Sweden","email":"anna@gmail.com","id":17,"name":"Anna"}

    REQUIRE(j.at("id") == 17);
    REQUIRE(j.at("age") == 42);
    REQUIRE(j.at("name") == "Anna Conda"s);
    REQUIRE(j.at("city") == "Uppsala"s);
}

TEST_CASE("convert list of person objects to json array") {
    using json = nlohmann::json;
    auto lst = std::vector<model::Person>{
            {1, "Anna"s,  21, "anna@rm.se"s,  "STH"s, "SE"s},
            {2, "Berit"s, 22, "berit@rm.se"s, "STH"s, "SE"s},
            {3, "Carin"s, 23, "carin@rm.se"s, "STH"s, "SE"s},
    };
    json j = lst;
    //std::cout << "JSON: " << j << "\n";
    //[{"age":21,"city":"STH","country":"SE","email":"anna@rm.se","id":1,"name":"Anna"},
    // {"age":22,"city":"STH","country":"SE","email":"berit@rm.se","id":2,"name":"Berit"},
    // {"age":23,"city":"STH","country":"SE","email":"carin@rm.se","id":3,"name":"Carin"}]

    REQUIRE(lst.size() == 3);
    REQUIRE(lst[0].name == "Anna"s);
    REQUIRE(lst[1].name == "Berit"s);
    REQUIRE(lst[2].name == "Carin"s);
}


