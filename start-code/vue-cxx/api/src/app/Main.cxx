#include <iostream>
#include <string>
#include "nlohmann/json.hpp"
#include "just_resting.hxx"
#include "cors.hxx"
#include "person.hxx"

using namespace std::literals;
namespace ws = just_resting;
using json = nlohmann::json;

int main() {
    auto repo = model::PersonRepo{"persons.csv"s};
    auto cors = CorsFilter{};

    auto srv = ws::Application{};
    srv.port(4500);
    srv.debug(false);
    srv.filter(&cors);

    srv.route("OPTIONS"s, "/"s, [](ws::Request& req, ws::Response& res) {});

    srv.route("GET"s, "/"s, [](ws::Request& req, ws::Response& res) {
        auto payload = json{
                {"name"s,    "API Server"s},
                {"version"s, "1.0"s},
        };
        res.body(payload.dump(2));
    });

    srv.route("GET", "/person", [&repo](ws::Request& req, ws::Response& res) {
        auto payload = repo.toJSON();
        res.body(payload.dump(2));
    });

    srv.route("GET", "/person/@", [&repo](ws::Request& req, ws::Response& res) {
        int id = req.param();
        auto payload = repo.toJSON(id);
        if (payload.empty()) {
            res.status(404, "object not found, id="s + std::to_string(id));
        } else {
            res.body(payload.dump(2));
        }
    });


    srv.launch([](auto port) {
        std::cout << "API server started on port " << port << "\n";
    });
}
