#pragma once
#include <string>
#include "just_resting.hxx"

using namespace std::literals;
namespace ws = just_resting;

struct CorsFilter : ws::Filter {
    void invoke(ws::Request& req, ws::Response& res, ws::Route& route) override {
        auto const method = req.method();
        res.header("Access-Control-Allow-Origin"s, "*"s);

        if (method == "OPTIONS"sv) {
            res.status(204, "No content"s);
            res.header("Content-Length"s, "0"s);

            auto methods = req.header("Access-Control-Request-Methods");
            if (methods) {
                res.header("Access-Control-Allow-Methods"s, std::string{methods->cbegin(), methods->cend()});
            } else {
                res.header("Access-Control-Allow-Methods"s, "GET, POST, PUT, DELETE"s);
            }

            auto headers = req.header("Access-Control-Request-Headers");
            if (headers) {
                res.header("Access-Control-Allow-Headers"s, std::string{headers->cbegin(), headers->cend()});
            }
            return;
        }

        next()->invoke(req, res, route);
    }
};

