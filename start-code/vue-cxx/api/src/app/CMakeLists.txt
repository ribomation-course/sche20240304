

add_library(person STATIC
        ${CMAKE_CURRENT_SOURCE_DIR}/person.hxx
        ${CMAKE_CURRENT_SOURCE_DIR}/person.cxx
)
target_compile_options(person PUBLIC ${WARN})
target_include_directories(person PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
)
target_link_libraries(person PUBLIC
        nlohmann_json::nlohmann_json
)


add_executable(api
        ${CMAKE_CURRENT_SOURCE_DIR}/cors.hxx
        ${CMAKE_CURRENT_SOURCE_DIR}/Main.cxx
)
target_compile_options(api PRIVATE ${WARN})
target_link_libraries(api PRIVATE
        person
        ribomation::just_resting
        nlohmann_json::nlohmann_json
)
target_link_options(api PRIVATE -static)


add_custom_command(TARGET api
        POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                "${CMAKE_CURRENT_SOURCE_DIR}/persons.csv"
                "$<TARGET_FILE_DIR:api>/persons.csv"
)

add_custom_command(TARGET api
        POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E make_directory
                "${CMAKE_SOURCE_DIR}/dist"
        COMMAND ${CMAKE_COMMAND} -E copy
                "${CMAKE_CURRENT_SOURCE_DIR}/persons.csv"
                "${CMAKE_SOURCE_DIR}/dist/persons.csv"
        COMMAND ${CMAKE_COMMAND} -E copy
                "$<TARGET_FILE:api>"
                "${CMAKE_SOURCE_DIR}/dist/api-server"
)

