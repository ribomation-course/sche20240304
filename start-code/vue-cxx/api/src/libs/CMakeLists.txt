include(FetchContent)

FetchContent_Declare(just_resting
        GIT_REPOSITORY https://github.com/ribomation/just_resting.git
        GIT_SHALLOW true
        GIT_TAG main
)
FetchContent_MakeAvailable(just_resting)

FetchContent_Declare(json
        URL https://github.com/nlohmann/json/releases/download/v3.11.3/json.tar.xz
)
FetchContent_MakeAvailable(json)

FetchContent_Declare(Catch2
        GIT_REPOSITORY https://github.com/catchorg/Catch2.git
        GIT_TAG v3.5.1
)
FetchContent_MakeAvailable(Catch2)
# link with: Catch2::Catch2WithMain
