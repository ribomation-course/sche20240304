import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {
        path: '/', name: 'home', component: () => import('../pages/home.page.vue')
    },
    {
        path: '/person-list', name: 'persons', component: () => import('../pages/person-list.page.vue')
    },
    {
        path: '/person/:id', name: 'person', component: () => import('../pages/person-view.page.vue')
    },
    {
        path: '/about', name: 'about', component: () => import('../pages/about.page.vue')
    },
];

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})

export default router
