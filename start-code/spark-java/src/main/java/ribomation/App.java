package ribomation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Date;
import java.util.Map;
import static spark.Spark.*;

public class App {
    static final String JSON = "application/json";
    static final Gson gson = new GsonBuilder()
            .setDateFormat("MMMM yyyy HH:mm")
            .setPrettyPrinting()
            .create();

    record Person(String name, int age, boolean female) { }

    public static void main(String[] args) {
        port(8000);

        get("/", JSON, (req, res) ->
                        Map.of("name", "Java Tiny Web (lean)",
                                "version", "2.42",
                                "date", new Date()),
                gson::toJson);

        post("/echo", JSON, (req, res) -> {
            var p = gson.fromJson(req.body(), Person.class);
            System.out.printf("** payload: %s%n", p);
            var reply = new Person(p.name().toUpperCase(), 10 * p.age(), !p.female());
            return Map.of("reply", reply);
        }, gson::toJson);
    }
}

