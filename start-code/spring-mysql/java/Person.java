package ribomation.webapp;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "persons")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    @Id private int id;
    private String name;
    private int age;
    private String email;
    private String city;
    private String country;
    private String icon;
    private String avatar;

    public String getIcon() {
        return image(email, 40);
    }

    public String getAvatar() {
        return image(email, 350);
    }

    @PostConstruct
    private void patch() {
        icon = image(email, 40);
        avatar = image(email, 350);
    }

    private static String image(String email, int size) {
        return String.format("https://robohash.org/%s.png?size=%dx%d", email, size, size);
    }
}
