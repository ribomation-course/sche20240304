package ribomation.webapp;

public class NotFound extends RuntimeException {
    public final Integer id;

    public NotFound(Integer id) {
        this.id = id != null ? id : -1;
    }
}
