package ribomation.webapp;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonDao extends JpaRepository<Person, Integer> {

    List<Person> findFirst10ByCountry(String country);

}
