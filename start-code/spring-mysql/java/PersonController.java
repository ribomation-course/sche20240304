package ribomation.webapp;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/person")
public class PersonController {
    private final PersonDao dao;

    public PersonController(PersonDao dao) {
        this.dao = dao;
    }

    @GetMapping
    public String index() {
        return "redirect:/person/list";
    }

    @GetMapping("list")
    public String list(Model m) {
        m.addAttribute("items", dao.findFirst10ByCountry("Sweden"));
        return "list";
    }

    @GetMapping("show/{id}")
    public String show(Model m, @PathVariable Integer id) {
        m.addAttribute("item", dao.findById(id).orElseThrow(() -> new NotFound(id)));
        return "show";
    }
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String error(Model m, NotFound x) {
        m.addAttribute("id", x.id);
        return "error";
    }

}
